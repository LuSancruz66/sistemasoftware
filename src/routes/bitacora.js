const express = require('express');
const bitSchema=require('../models/bitacora');
const cors = require('cors');


const router = express.Router();
router.use(cors());
//Registrar empleados en la bitacora

router.post('/bitacoraGuardar',  (req, res) => {
    const {noempleado, area, horain, horaout, idpersona}= req.body;
    
    if (!noempleado) {
        return res.status(401).json({
          msg: "El campo esta vacío"
        });
    
      } 


      if (!area) {
        return res.status(401).json({
          msg: "El campo esta vacío"
        });
    
      }
      if (!horain) {
        return res.status(401).json({
          msg: "El campo esta vacío"
        });
    
      }
      if (!horaout) {
        return res.status(401).json({
          msg: "El campo esta vacío"
        });
    
      }
      if (!idpersona) {
        return res.status(401).json({
          msg: "El campo esta vacío"
        });
    
      }

    const bit = new bitSchema({
        noempleado: noempleado,
        area: area,
        horain: horain,
        horaout: horaout,
        idpersona: idpersona
    });
    bit
        .save()
        .then((data) => {
            res.json({
                msg: "Bitacora agregada",
                data: data
            })
        }).catch((error) => {
            res.json({
                mensaje: error
            })
        }) 
})

//Mostrar empleados
router.get('/bitacora', async(req, res)=>{
    try {
        const bit= await bitSchema.find();
        console.log(bit)
        res.json(bit)

        console.log(bit)
    } catch (error) {
        res.json(error)
    } 
})

//Buscar empleado por id

router.get('/mostrar/:id', async (req, res)=>{
    try {
        const {id} = req.params;
        const bit = await bitSchema.findById(id)

        res.json(bit)
        
    } catch (error) {
         res.json(error);
    }
 })

 //Modificar bitacora
 router.put('/modificar/:id', async(req, res) => {

    const id = req.params.id;
    const {noempleado, area, horain, horaout, idpersona}= req.body;


    if (!noempleado) {
        return res.status(401).json({
          msg: "El campo esta vacío"
        });
    
      } 


      if (!area) {
        return res.status(401).json({
          msg: "El campo esta vacío"
        });
    
      }

      if (!horain) {
        return res.status(401).json({
          msg: "El campo esta vacío"
        });
    
      }
      if (!horaout) {
        return res.status(401).json({
          msg: "El campo esta vacío"
        });
    
      }
      if (!idpersona) {
        return res.status(401).json({
          msg: "El campo esta vacío"
        });
    
      }

    bitSchema.findByIdAndUpdate(id,{
        noempleado: noempleado,
        area: area,
        horain: horain,
        horaout: horaout,
        idpersona: idpersona

    },
    function (err, data) {
    if (err){
        res.json({
            msg: err
        })
    }
    else{
        res.json({
            msg: "Bitacora Actualizada",
            data: data
        });
    }
});
})

//Eliminar bitacora
router.delete('/eliminarBit/:id', (req, res) => {
    const id = req.params.id;
    bitSchema.findByIdAndDelete(id, function (err, data) {
        if (err){
            res.json({
                msg: err
            })
        }
        else{
            res.json({
                msg: "Bitacora Eliminada",
                data: data
            });
        }
    });
    
})

module.exports = router;