const mongodb = require('mongoose')

const bitSchema = new mongodb.Schema({
  
    "noempleado": {
        type: String,
        required: true,
        unique: true
    },
    "area":{
        type: String,
        required: true
    },
    "horain":{
        type: Number,
        required: true

    },
    "horaout":{
        type: Number,
        required: true
    },
    "idpersona":{
        type: String,
        required: true,
        unique: true
    }
    


})

module.exports= mongodb.model('Bitacora', bitSchema)