const mongodb = require('mongoose')

const userSchema = new mongodb.Schema({
    "nombre": {
        type: String, 
        required: true
    },
    "correo": {
        type: String, 
        required: true,
        unique: true
    },
    "password": {
        type: String,
        require: true,
        minlength: 6
    },
   
    role:{
        type: String,
        default: 'regular',
        enum: [
            'regular',
            'administrador'
        ]
    }
    


})

module.exports= mongodb.model('Persona', userSchema)