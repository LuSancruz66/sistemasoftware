const express = require('express');
const mongoose = require('mongoose');
//const bodyParser = require('body-parser');
//express

const rutaPersona = require('../src/routes/persona');
const rutaBitacora = require('../src/routes/bitacora');
/*const auth= require('./routes/auth')
const AuthToken = require('../src/middleware/authtoken')*/
require("dotenv").config();



const app = express();
const port = 3000;
app.use(express.json());
/*app.use(bodyParser.json());
app.use(AuthToken)
app.use(bodyParser.urlencoded({extended: false}));*/




//Rutas

app.use('/api', rutaPersona);
app.use('/api', rutaBitacora);
//app.use(auth);





mongoose.connect(process.env.DB_URI)
.then(() =>{
    console.log("Conexión éxitosa");
}).catch((error) =>{
    console.groupCollapsed(error);
})


app.listen(port, () =>  console.log(`Servidor arriba en el puerto: ${port}`))

